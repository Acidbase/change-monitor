package org.acidbase.cm;

import java.util.Date;

public class DelayException extends Exception
{
    private long millisec;
    private static Date previousOccurrence = new Date();
    private static int previousDelayIndex = -1;
    private static long[] intervals = { 10, 10, 20, 30, 50, 80, 130, 210, 340, 550, 890, 1440 };

    public static DelayException nextDelay()
    {
        Date currentTime = new Date();
        long ms = currentTime.getTime() - previousOccurrence.getTime();
        previousOccurrence = currentTime;
        if (previousDelayIndex == -1) {
            ms = ms < intervals[0] ? intervals[0] : 0l;
            previousDelayIndex = 0;
        }
        else {
            long errorInterval = ms - intervals[previousDelayIndex];
            previousDelayIndex = previousDelayIndex < intervals.length-1 ? previousDelayIndex+1 : previousDelayIndex;
            if (errorInterval <= intervals[previousDelayIndex]) {
                ms = intervals[previousDelayIndex];
            }
            else {
                ms = 0l;
                previousDelayIndex = -1;
            }
        }

        return new DelayException(ms);
    }

    private DelayException(long ms)
    {
        millisec = ms;
    }

    public void delay()
    {
        if (millisec > 0) {
            try {
                Thread.sleep(millisec);
            }
            catch (InterruptedException ignored) {
            }
        }
    }
}
