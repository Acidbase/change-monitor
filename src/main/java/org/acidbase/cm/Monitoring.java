package org.acidbase.cm;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import org.acidbase.cm.cache.ConnectorCache;
import org.acidbase.data.IndexStructure;
import org.acidbase.data.IndexStructureException;
import org.acidbase.data.mapper.Connector;
import org.acidbase.data.mapper.IndexStructureMapper;
import org.acidbase.data.structure.Structure;
import org.acidbase.data.structure.WritingStructure;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.indices.IndexAlreadyExistsException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public class Monitoring extends Thread
{
    private static final long LOCK_ID = 1l;
    private static DataMapper masterMapper = null;
    private static ConcurrentHashMap<String, String> activeDatabases = new ConcurrentHashMap<String, String>();
    private static AtomicBoolean exit = new AtomicBoolean(false);
    private static AtomicBoolean databaseMonitorExited = new AtomicBoolean(false);
    private static Connection connection = null;
    private static Channel channel = null;
    private static TransportClient es_client;
    static Logger logger = LoggerFactory.getLogger(Monitoring.class);

    public static void initializeDb()
    {
        HikariConnector masterConnector = new HikariConnector(Main.dbConnectionCreator);
        masterMapper = new DataMapper(masterConnector);
    }

    public static void initializeRmq()
        throws IOException, TimeoutException
    {
        connection = Main.factory.newConnection();
        channel = connection.createChannel();

        //Setting up the exchange and queue
        channel.exchangeDeclare(Main.worker_exchange, "direct", true, false, null);
        channel.queueDeclare(Main.worker_queue, true, false, false, null);
        channel.queueBind(Main.worker_queue, Main.worker_exchange, Main.index_entity_route_key);
        channel.exchangeDeclare(Main.broadcast_exchange, "fanout", false, false, null);
        channel.basicQos(1);
    }

    public static void initializeEs()
            throws DelayException
    {
        try {
            es_client = TransportClient
                .builder()
                .settings(Main.es_settings)
                .build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(Main.es_host), Main.es_port));
            if (es_client.connectedNodes().isEmpty()) {
                es_client.close();
                es_client = null;
                throw DelayException.nextDelay();
            }
        }
        catch (UnknownHostException ignored) {
            es_client = null;
            throw DelayException.nextDelay();
        }
    }

    public static void gracefullyExit()
    {
        exit.set(true);
        while (!exited()) {
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {}
        }
    }

    public static boolean exited()
    {
        return activeDatabases.size() == 0 && databaseMonitorExited.get();
    }

    private String databaseName;
    private Connector connector = null;
    private DataMapper dataMapper = null;

    @Override
    public void run()
    {
        //Check for new databases
        databaseName = null;
        while (!exit.get()) {
            if (es_client == null || es_client.connectedNodes().isEmpty()) {
                //Connect to Elasticsearch first
                while (!exit.get()) {
                    try {
                        initializeEs();
                        break;
                    }
                    catch (DelayException dex) {
                        dex.delay();
                    }
                }
            }

            try {
                databaseName = findNewDatabase();

                if (databaseName == null) {
                    try {
                        sleep(Main.sleepMillisec);
                    }
                    catch (InterruptedException ignored) {}
                }
                else {
                    break;
                }
            }
            catch (DelayException dex) {
                dex.delay();
            }
        }

        if (!exit.get()) {
            //Since there's been no exit signal, it means a new database is found
            try {
                masterMapper.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            logger.info("Started working on the database `{}`", databaseName);

            activeDatabases.put(databaseName, databaseName);
            try {
                connector = ConnectorCache.getConnector(databaseName);
                dataMapper = new DataMapper(connector);

                //Spawn a new thread to look for new databases to find, a replacement
                Monitoring replacement = new Monitoring();
                replacement.start();

                //Check for new records
                try {
                    while (!exit.get()) {
                        if (findNewRecords() == 0) {
                            try {
                                sleep(Main.sleepMillisec);
                            }
                            catch (InterruptedException e) {
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    logger.error(ex.toString());
                }

                //Close the database connection
                try {
                    dataMapper.close();
                }
                catch (SQLException ignored) {
                }
            }
            catch (ExecutionException ex) {
                logger.error(ex.toString());
            }

            //Reaching this point means that this database connection is lost / exit is signaled
            activeDatabases.remove(databaseName);

            logger.info("Stopped working on the database `{}`", databaseName);
        }
        else {
            databaseMonitorExited.set(true);

            logger.info("Database finder exited");
        }
    }

    private String findNewDatabase()
        throws DelayException
    {
        try {
            List<String> databases = masterMapper.getDatabaseList();
            if (databases.contains(Main.dbConnectionCreator.getDatabase())) {
                databases.remove(Main.dbConnectionCreator.getDatabase());
            }
            if (databases.size() > 0) {
                for (String db : databases) {
                    if (!activeDatabases.containsKey(db)) {
                        return db;
                    }
                }
            }
        }
        catch (SQLException ignored) {
            throw DelayException.nextDelay();
        }

        return null;
    }

    private int findNewRecords()
        throws TimeoutException, ParseException, IOException, SQLException, IndexStructureException, ExecutionException
    {
        Integer transactionKey = -1;
        try {
            dataMapper.obtainSessionLock(LOCK_ID);
            transactionKey = dataMapper.beginTransaction();
            List<DataMapper.ChangeInfo> records = dataMapper.getTopRecords(Main.recordsToLoad);
            List<DataMapper.ChangeInfo> recordsToRemove = new ArrayList<>();

            boolean anyStructureUpdate = false;
            for (DataMapper.ChangeInfo ci : records) {
                if (ci.revision_id == 0l) {
                    anyStructureUpdate = true;
                    break;
                }
            }
            if (!anyStructureUpdate) {
                dataMapper.releaseSessionLock(LOCK_ID);
            }

            JSONArray dataArray = new JSONArray();
            for (DataMapper.ChangeInfo ci : records) {
                if (ci.revision_id == 0l) {
                    if (dataArray.size() > 0) {
                        sendIndexMessage(dataArray);
                    }
                    //Update index structure only
                    try {
                        remapIndex(ci.index_name, ci.action);
                    }
                    catch (IndexAlreadyExistsException ignored) {}
                    if (channel == null || !channel.isOpen()) {
                        initializeRmq();
                    }
                    String msgJson = String.format("{ \"db\": \"%s\" }", databaseName);
                    channel.basicPublish(Main.broadcast_exchange, Main.reload_structure_route_key, MessageProperties.TEXT_PLAIN, msgJson.getBytes());
                }
                else {
                    //Index an entity
                    JSONObject data = new JSONObject();
                    data.put("entity_id", ci.entity_id);
                    data.put("revision_id", ci.revision_id);
                    data.put("action", ci.action);
                    data.put("entity_type", ci.entity_type);
                    data.put("index_name", ci.index_name);
                    dataArray.add(data);
                }
                recordsToRemove.add(ci);
            }

            if (dataArray.size() > 0) {
                sendIndexMessage(dataArray);
            }

            dataMapper.removeRecords(recordsToRemove);
            dataMapper.commit(transactionKey);
            if (anyStructureUpdate) {
                dataMapper.releaseSessionLock(LOCK_ID);
            }
            return records.size();
        }
        catch (Exception ex) {
            try {
                dataMapper.rollback(transactionKey);
            }
            catch (SQLException ignored) {}
            try {
                dataMapper.releaseSessionLock(LOCK_ID);
            }
            catch (SQLException ignored) {}
            throw ex;
        }
    }

    private void sendIndexMessage(JSONArray data)
        throws IOException, TimeoutException
    {
        JSONObject msgJson = new JSONObject();
        msgJson.put("db", databaseName);
        msgJson.put("data", data);

        if (channel == null || !channel.isOpen()) {
            initializeRmq();
        }
        channel.basicPublish(Main.worker_exchange, Main.index_entity_route_key, MessageProperties.TEXT_PLAIN, msgJson.toJSONString().getBytes());

        data.clear();
    }

    private void remapIndex(String index_name, String action)
        throws ParseException, IndexStructureException, SQLException, IOException, ExecutionException
    {
        if (action.toLowerCase().equals("delete")) {
            es_client
                .admin()
                .indices()
                .delete(new DeleteIndexRequest(databaseName + "_" + index_name))
                .actionGet();
        }
        else if (action.toLowerCase().equals("insert")) {
            IndexStructureMapper mapper = new IndexStructureMapper(connector);
            IndexStructure es = mapper.getIndexStructure(index_name);
            mapper.close();

            // Create the new mapping
            if (es != null) {
                XContentBuilder mappingBuilder = XContentFactory.jsonBuilder()
                    .startObject()
                    .startObject(index_name)
                    .startObject("properties");
                createElasticsearchMappingProperties(mappingBuilder, es.writingStructure);
                mappingBuilder
                    .endObject()
                    .endObject()
                    .endObject();

                CreateIndexRequestBuilder createIndexRequestBuilder = es_client
                    .admin()
                    .indices()
                    .prepareCreate(databaseName + "_" + index_name);
                createIndexRequestBuilder.addMapping(index_name, mappingBuilder);

                createIndexRequestBuilder
                    .execute()
                    .actionGet();
            }
        }
    }

    private void createElasticsearchMappingProperties(XContentBuilder mappingBuilder, WritingStructure es)
        throws IOException
    {
        if (es.revision_state != Structure.RevisionStateType.NotAnEntity) {
            mappingBuilder
                .startObject("entity_id")
                    .field("type", "long")
                .endObject()
                .startObject("revision_id")
                    .field("type", "long")
                .endObject()
                .startObject("creator")
                    .field("type", "long")
                .endObject()
                .startObject("reviser")
                    .field("type", "long")
                .endObject()
                .startObject("create_date")
                    .field("type", "date")
                    .field("format", "yyyy-MM-dd HH:mm:ss")
                .endObject()
                .startObject("revise_date")
                    .field("type", "date")
                    .field("format", "yyyy-MM-dd HH:mm:ss")
                .endObject();
        }
        else {
            mappingBuilder
                .startObject("object_id")
                .field("type", "long")
                .endObject();
        }

        mappingBuilder.startObject("fields");
            mappingBuilder.startObject("properties");
                for (Map.Entry<String, String> f : es.fields.entrySet()) {
                    String type = f.getValue().toLowerCase();
                    if (type.equals("date")) {
                        mappingBuilder
                            .startObject(f.getKey())
                                .field("type", "date")
                                .field("format", "yyyy-MM-dd")
                            .endObject();
                    }
                    else if (type.equals("datetime")) {
                        mappingBuilder
                            .startObject(f.getKey())
                                .field("type", "date")
                                .field("format", "yyyy-MM-dd HH:mm:ss")
                            .endObject();
                    }
                    else {
                        mappingBuilder
                            .startObject(f.getKey())
                                .field("type", type)
                            .endObject();
                    }
                }
            mappingBuilder.endObject();
        mappingBuilder.endObject();

        mappingBuilder.startObject("collections");
            mappingBuilder.startObject("properties");
                for (Map.Entry<String, WritingStructure> c : es.collections.entrySet()) {
                    mappingBuilder
                        .startObject(c.getKey())
                        .field("type", "nested")
                        .startObject("properties");
                    createElasticsearchMappingProperties(mappingBuilder, c.getValue());
                    mappingBuilder
                        .endObject()
                        .endObject();
                }
            mappingBuilder.endObject();
        mappingBuilder.endObject();
    }
}
