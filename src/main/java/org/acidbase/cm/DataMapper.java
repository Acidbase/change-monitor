package org.acidbase.cm;

import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.Connector;
import org.acidbase.data.mapper.Mapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataMapper extends Mapper
{
    public DataMapper(Connector connector)
    {
        super(connector);
    }

    public List<String> getDatabaseList()
        throws SQLException
    {
        Connection con = getConnection();
        PreparedStatement pst = con.prepareStatement("SELECT datname FROM pg_database WHERE datistemplate = false;");
        ResultSet rs = pst.executeQuery();
        List<String> databases = new ArrayList<>();
        while (rs.next()) {
            databases.add(rs.getString(1));
        }
        rs.close();
        pst.close();
        return databases;
    }

    public List<ChangeInfo> getTopRecords(int count)
        throws SQLException
    {
        Connection con = getConnection();
        PreparedStatement pst = con.prepareStatement(
            "SELECT cq.id, cq.entity_id, cq.revision_id, e.\"entity_type\", cq.\"action\", cq.index_name " +
            "FROM (SELECT * FROM \"Acidbase\".change_queue ORDER BY created LIMIT ? FOR UPDATE SKIP LOCKED) cq " +
            "LEFT JOIN \"Acidbase\".entity e ON (cq.entity_id = e.entity_id) " +
            "ORDER BY cq.id ASC "
        );
        pst.setInt(1, count);
        ResultSet rs = pst.executeQuery();
        List<ChangeInfo> records = new ArrayList<>();
        while (rs.next()) {
            ChangeInfo ci = new ChangeInfo();
            ci.id = rs.getLong(1);
            ci.entity_id = rs.getLong(2);
            ci.revision_id = rs.getLong(3);
            ci.entity_type = rs.getString(4);
            ci.action = rs.getString(5);
            ci.index_name = rs.getString(6);
            records.add(ci);
        }
        rs.close();
        pst.close();
        return records;
    }

    public int removeRecords(List<ChangeInfo> records)
        throws SQLException
    {
        if (records.size() == 0) {
            return 0;
        }

        Integer key = -1;
        try {
            List<String> ids = records.stream().map(ci -> ci.id.toString()).collect(Collectors.toList());
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("DELETE FROM \"Acidbase\".change_queue WHERE id IN (" + String.join(", ", ids) + ")");
            int affected = pst.executeUpdate();
            pst.close();
            commit(key);
            return affected;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public static class ChangeInfo
    {
        Long id;
        Long entity_id;
        Long revision_id;
        String entity_type;
        String action;
        String index_name;
    }
}
