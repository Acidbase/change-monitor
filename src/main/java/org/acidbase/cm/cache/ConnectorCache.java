package org.acidbase.cm.cache;

import org.acidbase.cm.HikariConnector;
import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.Connector;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;

import java.util.concurrent.ExecutionException;

public class ConnectorCache
{
    static final RemovalListener<String, HikariConnector> removalListener;
    static final LoadingCache<String, HikariConnector> connectors;
    static private ServerInfo serverInfo;
    static public void setServerInfo(ServerInfo serverInfo)
    {
        ConnectorCache.serverInfo = serverInfo;
    }

    static {
        removalListener = removal -> {
            HikariConnector connector = removal.getValue();
            connector.close();
        };

        connectors = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .removalListener(removalListener)
            .build(
                new CacheLoader<String, HikariConnector>() {
                    public HikariConnector load(String databaseName)
                    {
                        return new HikariConnector(serverInfo.cloneWithNewDb(databaseName));
                    }
                }
            );
    }

    static public Connector getConnector(String databaseName)
        throws ExecutionException
    {
        return connectors.get(databaseName);
    }
}
