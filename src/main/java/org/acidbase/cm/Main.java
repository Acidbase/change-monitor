package org.acidbase.cm;

import com.rabbitmq.client.ConnectionFactory;
import org.acidbase.cm.cache.ConnectorCache;
import org.acidbase.data.ServerInfo;
import org.elasticsearch.common.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class Main
{
    static Properties prop;

    static ServerInfo dbConnectionCreator;
    static long sleepMillisec = 1000;
    static int recordsToLoad = 10;

    static ConnectionFactory factory = null;
    static String worker_exchange = null;
    static String worker_queue = null;
    static String broadcast_exchange = null;
    static String index_entity_route_key = null;
    static String reload_structure_route_key = null;

    static Settings es_settings;
    static String es_host = null;
    static int es_port = 0;

    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] argv)
        throws IOException, SQLException, ExecutionException, TimeoutException
    {
        InputStream input = null;
        input = Main.class.getClassLoader().getResourceAsStream("config.properties");
        if (input == null) {
            logger.error("Unable to find \"config.properties\"");
            return;
        }

        prop = new Properties();
        prop.load(input);

        sleepMillisec = Long.parseLong(prop.getProperty("monitoring.sleep_millisec").trim());
        recordsToLoad = Integer.parseInt(prop.getProperty("monitoring.number_of_records_in_batch").trim());

        worker_exchange = prop.getProperty("rmq.exchange.worker").trim();
        worker_queue = prop.getProperty("rmq.queue.worker").trim();
        broadcast_exchange = prop.getProperty("rmq.exchange.broadcast").trim();
        index_entity_route_key = prop.getProperty("rmq.route_key.index_entity").trim();
        reload_structure_route_key = prop.getProperty("rmq.route_key.reload_structure").trim();

        /**
         * =============================================================================================================
         * Preparing the PostgreSQL connection
         * =============================================================================================================
         */
        dbConnectionCreator = new ServerInfo(
            prop.getProperty("db.protocol").trim(),
            prop.getProperty("db.type").trim(),
            prop.getProperty("db.host").trim(),
            Integer.parseInt(prop.getProperty("db.port").trim()),
            prop.getProperty("db.name").trim(),
            prop.getProperty("db.user").trim(),
            prop.getProperty("db.pass").trim()
        );
        ConnectorCache.setServerInfo(dbConnectionCreator);


        /**
         * =============================================================================================================
         * RabbitMQ setup
         * =============================================================================================================
         */
        factory = new ConnectionFactory();
        factory.setHost(prop.getProperty("rmq.host").trim());
        factory.setPort(Integer.parseInt(prop.getProperty("rmq.port").trim()));
        String rmq_user = prop.getProperty("rmq.username").trim();
        if (!rmq_user.equals("")) {
            factory.setUsername(rmq_user);
        }
        String rmq_pass = prop.getProperty("rmq.password").trim();
        if (!rmq_pass.equals("")) {
            factory.setUsername(rmq_pass);
        }


        /**
         * =============================================================================================================
         * Elasticsearch setup
         * =============================================================================================================
         */
        es_host = prop.getProperty("es.host").trim();
        es_port = Integer.parseInt(prop.getProperty("es.port").trim());
        es_settings = Settings
            .settingsBuilder()
            .put("cluster.name", prop.getProperty("es.cluster_name").trim())
//            .put("node.name", prop.getProperty("es.node_name").trim())
            .build();


        /**
         * =============================================================================================================
         * Handler for shutting down gracefully
         * =============================================================================================================
         */
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                logger.info("Shutdown hook called");
                Monitoring.gracefullyExit();
            }
        });


        /**
         * =============================================================================================================
         * Starting the process
         * =============================================================================================================
         */
        Monitoring.initializeDb();
        Monitoring firstMonitor = new Monitoring();
        firstMonitor.start();

        while (!Monitoring.exited()) {
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {}
        }

        logger.info("Main thread exited");
    }
}
